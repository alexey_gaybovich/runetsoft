<?php
/**
 * Range-Ray(tm) : runetsoft (https://rangeray.ru)
 * Copyright (c) 2013-2019 Range-Ray web development, Inc. (https://rangeray.ru)
 *
 * Licensed commercial license
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Range-Ray web development, Inc. (https://rangeray.ru)
 * @link          https://rangeray.ru Range-Ray(tm) runetsoft
 * @since         0.0.0
 * @author     gaibovich.alexey <rangeray@mail.ru>
 * @license  commercial license
 * @package runetsoft
 * @var $product \RuNetSoft\TestApp\Model\Entity\ProductEntity
 */

use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

require_once dirname( __DIR__) . DIRECTORY_SEPARATOR . 'config/bootstrap.php'; // загрузка приложения

if(Configure::read('Debug')){
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}


$ProductsTable = TableRegistry::getTableLocator()->get('Products', [
    'className' => 'RuNetSoft\TestApp\Model\Table\ProductsTable'
]);

$query = $ProductsTable->find()->contain(['Specifications'])->toArray();


try {
    foreach ($query as $product) {

        $specification = $product->getSpecification($ProductsTable);
        if($ProductsTable->Specifications->save($specification)){
            $product->set('not_parsed', 0);
            $ProductsTable->save($product);
            Log::write('debug', 'parse success: '. PHP_EOL . $specification . PHP_EOL . $product);

        } else {
            $product->set('not_parsed', 1);
            $ProductsTable->save($product);
            \Cake\Log\Log::write('error', 'parse error: '. PHP_EOL . $specification . PHP_EOL . $product);

        }

        debug($product, 'print_r');
        debug($specification, 'print_r');
    }
} catch (Exception $exception) {
    \Cake\Log\Log::write('error', 'application error: '. PHP_EOL . $exception);
}





