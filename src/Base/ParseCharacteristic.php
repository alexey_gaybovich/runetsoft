<?php


namespace RuNetSoft\TestApp\Base;


/**
 * Class ParseCharacteristic
 * @package RuNetSoft\TestApp\Base
 */
class ParseCharacteristic implements ParserInterface
{

    /**
     * (\s+)((Run Flat)?(\s+)?[*]?\s+(TL).+) v 1
     * (.+?)(((ROF)|(TL)).+) v 2
    */
    private static $parse_string = null;

    /**
     * @var array
     */
    private static $_defaultConfig = [
        'template' => [
            'brand' => '~(Nokian|BFGoodrich|Pirelli|Toyo|Continental|Hankook|Mitas)(.+)~u',
            'model' => '~\s+(.*?)\s+(\d+/.+)~u',
            'width' => '~(\d+)(.+)~u',
            'height' => '~/(\d+)(.+)~u',
            'design' => '~(R)(.+)~u',
            'diameter' => '~(\d+)(.+)~u',
            'loadidx' => '~(\d+/\d+|\d+)(.+)~u',
            'speedidx' => '~([^\s\d])(.+)~u',
            'charabbr' => '~(.+?)(((RunFlat|Run Flat|ROF|ZP|SSR|ZPS|HRS|RFT)|(TT|TL|TL\/TT)).+)~m',
            'rof' => '~(RunFlat|Run Flat|ROF|ZP|SSR|ZPS|HRS|RFT)(.+)~m',
            'tirechamb' => '~(TT|TL|TL\/TT)(.+)~m',
            'season' => '~(Зимние \(шипованные\)|Внедорожные|Летние|Зимние \(нешипованные\)|Всесезонные)~m'
        ]
    ];

    /**
     * @param string $string
     * @return array
     */
    public static function getParams(string $string)
    {
        return array_map(function ($row) {

        }, static::$_defaultConfig['template']);
    }

    /**
     * @param string $param
     * @param string $string
     * @param bool $use_buffer
     * @return mixed|null
     */
    public static function getParam(string $param, string $string, bool $use_buffer = false)
    {
        $template = static::$_defaultConfig['template'];

        if($use_buffer && !empty(static::$parse_string)){ // если есть параметр буфера берем строку из него
            $string = static::getStringFromBuffer();
        }

        if (array_key_exists($param, $template)) {

            $reg = $template[$param];

            preg_match($reg, $string, $matches);

            if ($use_buffer && !empty($matches[1]) && !empty($matches[2])) {
                static::setStringToBuffer($matches[2]);
            }


            return empty($matches[1]) ? null : trim($matches[1]);
        }
        return null;
    }

    /**
     * @return null
     */
    public static function getStringFromBuffer()
    {
        return static::$parse_string;
    }

    /**
     * @param string $string
     */
    public static function setStringToBuffer(string $string)
    {
        static::$parse_string = $string;
    }

    /**
     *
     */
    public static function cleanBuffer()
    {
        static::$parse_string = null;
    }


}