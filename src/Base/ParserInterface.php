<?php


namespace RuNetSoft\TestApp\Base;


use Cake\ORM\Entity;

interface ParserInterface
{

    public static function getParams(string $string);

    public static function getParam(string $param, string $string, bool $use_buffer = false);

    public static function getStringFromBuffer();

    public static function setStringToBuffer(string $string);

    public static function cleanBuffer();
}