<?php


namespace RuNetSoft\TestApp\Model\Entity;


use Cake\ORM\Association\HasOne;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use RuNetSoft\TestApp\Base\ParseCharacteristic;
use RuNetSoft\TestApp\Base\ParserInterface;
use RuNetSoft\TestApp\Model\Table\ProductsTable;
use RuNetSoft\TestApp\Model\Table\SpecificationsTable;

/**
 * @property int $product_id
 * @property string $product_name
*/

class ProductEntity extends Entity
{
    private $properties = [
        'brand',
        'model',
        'width',
        'height',
        'design',
        'diameter',
        'loadidx',
        'speedidx',
        'charabbr',
        'rof',
        'tirechamb',
        'season',
    ];

    protected $_accessible = [
        'product_name' => true,
        'product_id' => false
    ];

    /**
     * Фабрика характеристик товара
    */
    public function getSpecification(ProductsTable $ProductsTable)
    {



        $data = ['product_id' => $this->get('product_id')];

        foreach ($this->properties as $property) {

            $data[$property] = ParseCharacteristic::getParam($property, $this->product_name, true);
        }

        /**
         * если есть у товара характеристики то берем сущьность характеристик из него
        */

        $specification = $this->get('specification') instanceof SpecificationEntity ? $this->get('specification') : $ProductsTable->Specifications->newEntity();


        $ProductsTable->Specifications->patchEntity($specification, $data);

        //$specification->set('original_product_name', $this->get('product_name'));
        //$specification->set('remainder_parse', ParseCharacteristic::getStringFromBuffer());

        ParseCharacteristic::cleanBuffer(); // очищаем буфер


        return $specification;
    }

}