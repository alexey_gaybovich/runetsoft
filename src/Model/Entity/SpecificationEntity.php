<?php


namespace RuNetSoft\TestApp\Model\Entity;


use Cake\ORM\Entity;

class SpecificationEntity extends Entity
{
    protected $_accessible = [
        '*' => true,
        'specification_id' => false
    ];
}