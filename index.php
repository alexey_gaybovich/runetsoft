<?php
/**
 * Range-Ray(tm) : runetsoft (https://rangeray.ru)
 * Copyright (c) 2013-2019 Range-Ray web development, Inc. (https://rangeray.ru)
 *
 * Licensed commercial license
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Range-Ray web development, Inc. (https://rangeray.ru)
 * @link          https://rangeray.ru Range-Ray(tm) runetsoft
 * @since         0.0.0
 * @author     gaibovich.alexey <rangeray@mail.ru>
 * @license  commercial license
 * @package runetsoft
 */
require 'webroot' . DIRECTORY_SEPARATOR . 'index.php';