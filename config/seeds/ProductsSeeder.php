<?php


use Phinx\Seed\AbstractSeed;

class ProductsSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {

        $handle = fopen(__DIR__ . "/data.txt", "r");
        if ($handle) {
            $data = [];

            while (($line = fgets($handle)) !== false) {
                $data[] = [
                    'product_name' => $line
                ];
            }

            fclose($handle);

            $posts = $this->table('products');
            $posts->insert($data)->save();

        } else {

            echo 'error reading file';
        }

    }
}
