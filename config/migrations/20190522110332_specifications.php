<?php

use Phinx\Migration\AbstractMigration;

class Specifications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('specifications', [
            'id' => 'specification_id',
            'limit' => 10,
            'null' => false,
            'signed' => false,
        ]);
        $table->addColumn('product_id', 'integer', [
            'default' => null,
            'limit' => 10,
            'null' => false,
            'signed' => false,
        ]);
        $table->addColumn('brand', 'string', [
            'default' => null,
            'limit' => 256,
            'null' => true,
        ]);
        $table->addColumn('model', 'string', [
            'default' => null,
            'limit' => 256,
            'null' => true,
        ]);
        $table->addColumn('width', 'integer', [
            'default' => null,
            'limit' => 10,
            'null' => true,
            'signed' => false,
        ]);
        $table->addColumn('height', 'integer', [
            'default' => null,
            'limit' => 10,
            'null' => true,
            'signed' => false,
        ]);
        $table->addColumn('design', 'string', [
            'default' => null,
            'limit' => 256,
            'null' => true,
        ]);
        $table->addColumn('diameter', 'integer', [
            'default' => null,
            'limit' => 3,
            'null' => true,
            'signed' => false,
        ]);
        $table->addColumn('loadidx', 'integer', [
            'default' => null,
            'limit' => 3,
            'null' => true,
            'signed' => false,
        ]);
        $table->addColumn('speedidx', 'string', [
           'default' => null,
           'limit' => 256,
           'null' => true,
       ]);
        $table->addColumn('charabbr', 'string', [
           'default' => null,
           'limit' => 256,
           'null' => true,
       ]);
        $table->addColumn('rof', 'string', [
           'default' => null,
           'limit' => 256,
           'null' => true,
       ]);
        $table->addColumn('tirechamb', 'string', [
           'default' => null,
           'limit' => 256,
           'null' => true,
       ]);
        $table->addColumn('season', 'string', [
           'default' => null,
           'limit' => 256,
           'null' => true,
       ]);
        $table->create();
    }

    public function down()
    {
        $this->table('specifications')
            ->drop()
            ->save();
    }
}
