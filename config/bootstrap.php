<?php
/**
 * Range-Ray(tm) : runetsoft (https://rangeray.ru)
 * Copyright (c) 2013-2019 Range-Ray web development, Inc. (https://rangeray.ru)
 *
 * Licensed commercial license
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Range-Ray web development, Inc. (https://rangeray.ru)
 * @link          https://rangeray.ru Range-Ray(tm) runetsoft
 * @since         0.0.0
 * @author     gaibovich.alexey <rangeray@mail.ru>
 * @license  commercial license
 * @package runetsoft
 */

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

define('DS', DIRECTORY_SEPARATOR);
define('APP', dirname(__DIR__) . DS);
define('WWW_ROOT', APP . 'webroot' . DS);
define('CONFIG_PATH', APP . 'config' . DS);
define('TMP', APP . 'tmp' . DS);
define('LOGS', APP . 'logs' . DS);
define('CACHE', TMP . 'cache' . DS);

require_once APP . 'lib/lib.php'; // небольшие функции общая библиотека

require_once APP . 'vendor/autoload.php';

Configure::write(include(CONFIG_PATH . 'app.php')); // конфигурируем приложение

Cache::setConfig(Configure::consume('Cache'));
Log::setConfig(Configure::consume('Log'));
ConnectionManager::setConfig(Configure::consume('Datasources'));