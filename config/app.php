<?php
/**
 * Range-Ray(tm) : runetsoft (https://rangeray.ru)
 * Copyright (c) 2013-2019 Range-Ray web development, Inc. (https://rangeray.ru)
 *
 * Licensed commercial license
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Range-Ray web development, Inc. (https://rangeray.ru)
 * @link          https://rangeray.ru Range-Ray(tm) runetsoft
 * @since         0.0.0
 * @author     gaibovich.alexey <rangeray@mail.ru>
 * @license  commercial license
 * @package runetsoft
 */
return [
    'App' => [
        'namespace' => 'RuNetSoft\TestApp',
        'encoding' => env('APP_ENCODING', 'UTF-8'),
        'defaultLocale' => env('APP_DEFAULT_LOCALE', 'ru_RU'),
        'base' => false,
        'dir' => 'src',
    ],
    'Debug' => filter_var(env('APP_DEBUG', true), FILTER_VALIDATE_BOOLEAN),
    'Datasources' => [
        'default' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => env('APP_DB_HOST', '127.0.0.1'),
            'port' => env('APP_DB_PORT', '3306'),
            'username' =>  env('APP_DB_USERNAME','change_me'),
            'password' =>  env('APP_DB_PASSWORD', 'change_me'),
            'database' =>  env('APP_DB_NAME', 'change_me'),
            'encoding' => 'utf8',
            'timezone' => '',
            'flags' => [],
            'cacheMetadata' => true,
            'log' => false,
            'url' => env('APP_DB_URL', null),
        ],

    ],
    'Cache' => [
        'default' => [
            'className' => 'File',
            'path' => CACHE,
            'url' => env('CACHE_DEFAULT_URL', null),
        ],

        '_cake_core_' => [
            'className' => 'File',
            'prefix' => 'runetsoft_cake_core_',
            'path' => CACHE . 'persistent/',
            'serialize' => true,
            'duration' => '+2 minutes',
            'url' => env('CACHE_CAKECORE_URL', null),
        ],

        '_cake_model_' => [
            'className' => 'File',
            'prefix' => 'runetsoft_cake_model_',
            'path' => CACHE . 'models/',
            'serialize' => true,
            'duration' => '+2 minutes',
            'url' => env('CACHE_CAKEMODEL_URL', null),
        ],
    ],
    'Log' => [
        'debug' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'debug',
            'levels' => ['notice', 'info', 'debug'],
            'url' => env('LOG_DEBUG_URL', null),
        ],
        'error' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'error',
            'levels' => ['warning', 'error', 'critical', 'alert', 'emergency'],
            'url' => env('LOG_ERROR_URL', null),
        ],
    ],

];